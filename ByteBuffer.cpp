#include<string>
#include<iostream>
#include <byteswap.h> 


#include "lang/lang.h"

#include "include/ByteBuffer.h"
using namespace std; 
	ByteBuffer::ByteBuffer(char * bp){
			buffer=bp; 
		}
		void ByteBuffer::skip(int bytes){
			buffer+=bytes;
		}
		char ByteBuffer::get(){
			char c=buffer[0];
			buffer++;
			return c;
		}
		short ByteBuffer::getShort(){
			short * ib=(short *)buffer;
			short i=ib[0];
			buffer += 2;
			return bswap_16(i);
		}
		unsigned short ByteBuffer::getUShort(){
			unsigned short * ib=(unsigned short *)buffer;
			unsigned short i=ib[0];
			buffer += 2;
			return bswap_16(i);
		}
		float ByteBuffer::getFloat(){
			float * ib=(float *)buffer;
			float i=ib[0];
			buffer += 4;
			return bswap_32(i);
		}
		int ByteBuffer::getInt(){
			int * ib=(int *)buffer;
			int i=ib[0];
			buffer += 4;
			return bswap_32(i);
		}
		double ByteBuffer::getDouble(){
			double * ib=(double *)buffer;
			double i=ib[0];
			buffer += 8;
			return bswap_64(i);
		}
		long ByteBuffer::getLong(){
			long * ib=(long *)buffer;
			long i=ib[0];
			buffer += 8;
			return bswap_64(i);
		}
