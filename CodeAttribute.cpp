#include <string>
#include <iostream>

#include "lang/lang.h"

#include "include/ByteBuffer.h"
#include "include/Attribute.h"
#include "include/MException.h"
#include "include/CodeAttribute.h"
#include "include/Constant.h"
#include "include/Field.h"
#include "include/Method.h"
#include "include/Frame.h"
#include "include/Class.h"

using namespace std;

//static
CodeAttribute * CodeAttribute::read(String * name, Class * cl, ByteBuffer *  buf)
{
	int attribute_length = buf->getInt();
		unsigned short max_stack = buf->getUShort();
		unsigned short max_locals = buf->getUShort();
		CodeAttribute * ca = new CodeAttribute(max_stack,max_locals);

		int code_length = buf->getInt();
		ca->code = new unsigned char[code_length];
		for(int i=0;i<code_length;i++)
			ca->code[i]=buf->get();

		unsigned short exception_table_length = buf->getUShort();

		ca->exception_table = new MException*[exception_table_length];
		for (int i = 0; i < exception_table_length; i++) {
			MException * ex=new MException();
			ex->start_pc = buf->getUShort();
			ex->end_pc = buf->getUShort();
			ex->handler_pc = buf->getUShort();
			ex->catch_type = buf->getUShort();
			ca->exception_table[i] = ex;
		}

		unsigned short attributes_count = buf->getUShort();
		for (int ix = 0; ix < attributes_count; ix++) {
			Attribute *  attr = Attribute::generate(cl,buf);
			/*
			if (name.equals("LineNumberTable")) {
				line_number_table = new LineNumberTableAttribute(attr);
			}
			if (name.equals("LocalVariableTable")) {
				local_variable_table = new LocalVariableTableAttribute(attr);
			}
			if (name.equals("LocalVariableTypeTable")) {
				local_variable_type_table = new LocalVariableTypeTableAttribute(attr);
			}
			*/

		}
		return ca;
}

