#include <string>
#include <iostream>

#include "lang/lang.h"

#include "include/ByteBuffer.h"
#include "include/Attribute.h"
#include "include/MException.h"
#include "include/CodeAttribute.h"
#include "include/Constant.h"
#include "include/Field.h"
#include "include/Method.h"
#include "include/Frame.h"
#include "include/Class.h"
#include "include/Frame.h"
#include "include/FrameStack.h"
#include "include/Instance.h"
#include "include/Heap.h"
#include "include/Executor.h"
#include "include/Thread.h"



Thread::Thread(Method * m){
	heap=new Heap();
	e=new Executor(heap,m);
}
void Thread::run(){
	e->step();
}
