#include <string>
#include <iostream>
#include <fstream>
#include <stdint.h>
#include <inttypes.h>
#include <arpa/inet.h>
#include <byteswap.h> 

#include "lang/lang.h"

#include "include/ByteBuffer.h"
#include "include/Constant.h"
#include "include/Field.h"
#include "include/Attribute.h"
#include "include/MException.h"
#include "include/CodeAttribute.h"
#include "include/Method.h"
#include "include/Frame.h"
#include "include/Class.h"
using namespace std;

HashMap<String,Class> * Class::classes=new HashMap<String,Class>();

Class::Class(String classname) {
	std::ifstream is (*(classname+*_(".class")).to_cpp_string(), std::ifstream::binary);
	if (!is) return;
	// get length of file:
	is.seekg (0, is.end);
	int length = is.tellg();
	is.seekg (0, is.beg);

	// ...buffer contains the entire file...
	char * buffer = new char[length];

	// read data as a block:
	is.read (buffer,length);

	if (!is)
		std::cout << "error: only " << is.gcount() << " could be read";
	is.close();
	ByteBuffer * buf = new ByteBuffer(buffer);

	if(buf->getInt()!=HEAD)
	{
		std::cout << "not a class file"<<endl;
		return;
	}
	short minor = buf->getShort();//bswap_16(sp[2]);
	short ver = buf->getShort();//bswap_16(sp[3]);
	//std::cout << "class file version: "<<ver<<"."<<minor<< endl;
	short constant_pool_count = buf->getShort();
	constants=new Array<Constant>(constant_pool_count);
	//std::cout << "constant_pool_count: "<<constant_pool_count<< endl;
	for(int i=1;i<constant_pool_count;i++){
		cout<<"constant "<<i<<":";
		Constant * c=new Constant(buf);
		/*Ü
			if (c.getTag() == Constant.CLong || c.getTag() == Constant.CDouble) {
				ix++;
				constants.add(c);
			}
			*/
		constants->set(i,c);
	}
	unsigned short access_flags = buf->getUShort();


	this_class = buf->getUShort();

	name = getConstant(getConstant(this_class)->index1)->str;
	cout<<"name::::"<<*name<<endl;
	super_class = buf->getUShort();


	unsigned short interfaces_count = buf->getUShort();
	std::cout << "interfaces_count "<<interfaces_count<<endl;
	unsigned short * interfaces=new unsigned short[interfaces_count];
	for (int i = 0; i < interfaces_count; i++) {
		interfaces[i] = buf->getUShort();
	}

	unsigned short fields_count = buf->getUShort();
	fields=new Array<Field>(fields_count);
	std::cout << "fields_count "<<fields_count<<endl;
	for (int i = 0; i < fields_count; i++) {
		Field * f = new Field(this,buf);
		fields->set(i,f);
		//fields.put(f.getName(), f);
		/*
		   if (f.isStatic()) {
		   putStatic(f.getName(), f.getDefaultValue());
		   if(f.isObject()) {
		   static_references.add(f.getName());
		//if (!f.isFinal())
		hasStaticNonFinalReferences = true;
		}

		}
		else
		if(f.isObject())
		references.add(f.getName());
		*/
	}
	unsigned short methods_count = buf->getUShort();
	methods = new Array<Method>(methods_count);
	std::cout << "methods_count "<<methods_count<<endl;
	_methods=new HashMap<String,Method>();
	for (int i = 0; i < methods_count; i++) {
		Method * m = new Method(this, buf);
		methods->set(i,m);
		String * signature=m->getSignature();
		cout<<*signature<<endl;
		_methods->put(signature,m);
	}

	
	unsigned short attributes_count = buf->getUShort();
	std::cout << "attributes_count "<<attributes_count<<endl;
	for (int i = 0; i < attributes_count; i++) {
		Attribute * a = Attribute::generate(this,buf);
		/*
		   if(a.getName(this).equals("SourceFile")){
		   source_file=new SourceFileAttribute(a,this).getSourceFileName();
		   }*/

		//attrs.add(a);
	}

	delete[] buffer;
}

Constant * Class::getConstant(unsigned short i){
	return constants->get(i);
}

Method * Class::resolveMethod(unsigned short i){
	Constant * mc = getConstant(i);
	int cref = mc->index1;
	int mref = mc->index2;

	int nid = getConstant(mref)->index1;
	int tid = getConstant(mref)->index2;
	String * mname = getConstant(nid)->str;
	String * parameters = getConstant(tid)->str;
	String signature = *mname+*parameters;
	if (cref == this_class) {
		return getMethod(&signature);
	} else {

		return resolveClass(cref)->getMethod(&signature);
	}
	return 0;
}

void Class::ldc(unsigned short i, Frame * cF){
	Constant * c=getConstant(i);
	cF->push(c->value);
}

void Class::getStatic(unsigned short i, Frame * cF){

}

void Class::ldc2(unsigned short i, Frame * cF){
	Constant * c=getConstant(i);
	cF->pushl(c->lvalue);
}
Method * Class::getMethod(String * signature){
	return _methods->get(signature);
}

Class * Class::resolveClass(unsigned short cref){
	cout<<"cref "<<cref<<endl;
	Constant * c = getConstant(cref);
	cout<<"index1 "<<c->index1<<endl;
	c = getConstant(c->index1);
	String * s = c->str;
	cout<<*s<<endl;
	return getClass(s);
}
//static
Class * Class::getClass(String * classname){
	if(classes->containsKey(classname))
	{
		return classes->get(classname);
	}
	Class * cl=new Class(*classname);
	classes->put(classname,cl);
	return cl;
}
