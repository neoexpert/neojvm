#include <string>
#include <iostream>

#include "lang/lang.h"

#include "include/ByteBuffer.h"
#include "include/Constant.h"
#include "include/Attribute.h"
#include "include/MException.h"
#include "include/CodeAttribute.h"
#include "include/Field.h"
#include "include/Method.h"
#include "include/Frame.h"
#include "include/Class.h"


Method::Method(Class * cl, ByteBuffer * buf){
	this->cl=cl;
	access_flags = buf->getUShort();
	unsigned short name_index = buf->getUShort();

	name = cl->getConstant(name_index)->str;

	unsigned short descriptor_index = buf->getUShort();

	String * desc = cl->getConstant(descriptor_index)->str;
	args=parseParams(desc);
	cout<<"params count:"<<paramscount<<endl;
	signature=*name+*desc;


	int attributes_count = buf->getUShort();
	//attrs = new ArrayList<>(attributes_count);
	for (int ix = 0; ix < attributes_count; ix++) {
		Attribute * a = Attribute::generate(cl, buf);
		if(CodeAttribute * ca = dynamic_cast<CodeAttribute*>(a)){
			codeattr=ca;
		}
	}
}

int Method::getParamsArrayLength(){
	return args->size();
}
ArrayList<Character> * Method::parseParams(String * desc){
	desc = desc->substring(1);
	ArrayList<Character> * params = new ArrayList<Character>(16);

	while (true) {
		Character * c = desc->charAt(0);
		if (*c == ')')
			break;
		switch (*c) {
			case 'Z':
			case 'B':
			case 'C':
			case 'S':
			case 'I':
			case 'F':
				params->add(c);
				break;
			case 'D':
			case 'J':
				params->add(c);
				params->add(c);
				break;
			case 'L':
				params->add(c);
				desc = desc->substring(desc->indexOf(';') + 1);
				continue;
			case '[':
				params->add(c);
				Character * at = desc->charAt(1);
				while(*at=='['){
					desc=desc->substring(1);
					at = desc->charAt(1);

				}
				if(*at=='L')
					desc = desc->substring(desc->indexOf(';') + 1);
				else
					desc = desc->substring(2);
				continue;
		}
		desc = desc->substring(1);
	}
	paramscount=params->size();
	return params;
}
