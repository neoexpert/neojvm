#ifndef _Field_h_
#define _Field_h_

using namespace std; 

class Class;
class Field:public Object
{ 
	private:
		unsigned short access_flags;
    public: 
    	Field(Class * c, ByteBuffer * buf);
};
#endif
