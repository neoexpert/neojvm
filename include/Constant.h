#ifndef _Constant_h_
#define _Constant_h_

using namespace std; 

class Constant:public Object
{ 
	private:
		void decodeString(ByteBuffer * bb);
    public: 
		String * str;
		int value;
		long lvalue;
		char tag;
		int index1;
		int index2;
		static const char CUtf8 = 1;
		static const char CInteger = 3;
		static const char CFloat = 4;
		static const char CLong = 5;
		static const char CDouble = 6;
		static const char CClass = 7;
		static const char CString = 8;
		static const char CFieldRef = 9;
		static const char CMethodRef = 10;
		static const char CInterfaceMethodRef = 11;
		static const char CNameAndType = 12;
		static const char CMethodHandle = 15;
		static const char CMethodType = 16;
		static const char CInvokeDynamic = 18;

    	Constant(ByteBuffer * bb);
};
#endif
