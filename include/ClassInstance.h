#ifndef _ClassInstance_h_
#define _ClassInstance_h_


class ClassInstance:public Instance
{ 
	private:
		Class * cl;
	public:
		ClassInstance(Class * cl);
		virtual Class * getClass(){return cl;}
		void putField(String * name, Object * value);
		Object * getField(String * name);
};
#endif
