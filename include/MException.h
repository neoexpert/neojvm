#ifndef _MException_h_
#define _MException_h_

using namespace std; 

class MException:public Object
{ 
    public:
	unsigned short start_pc;

	unsigned short end_pc;

	unsigned short handler_pc;

	unsigned short catch_type;
};
#endif
