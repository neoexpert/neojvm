#ifndef _Instance_h_
#define _Instance_h_


class Instance:public Object
{ 
	private:
	public:
		Instance();
		virtual Class * getClass()=0;
};
#endif
