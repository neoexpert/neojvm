#ifndef _Heap_h_
#define _Heap_h_


class Heap:public Object
{ 
	private:
	public:
		Heap();
		int add(Instance * i);
		Instance * get(int addr);
};
#endif
