#ifndef _Class_h_
#define _Class_h_
using namespace std;
class Class:public Object
{
	private:
		static const int HEAD = 0xcafebabe;
		unsigned short this_class=0;
		unsigned short super_class=0;
		String * name;
		Array<Field> * fields;
		HashMap<String,Method> * _methods;
		static HashMap<String,Class> * classes;
	public:
		Array<Constant> * constants;
		Array<Method> * methods;
		Class(String classname);
		Constant * getConstant(unsigned short i);
		String * getName(){return name;}
		void ldc(unsigned short i,Frame * cF);
		void ldc2(unsigned short i,Frame * cF);
		void getStatic(unsigned short i,Frame * cF);
		Method * getMethod(String * signature);
		Class * resolveClass(unsigned short int);
		Method * resolveMethod(unsigned short i);
		static Class * getClass(String * classname);
};
#endif
