#ifndef _Frame_h_
#define _Frame_h_


class Frame:public Object
{ 
	private:
		Method * m;
		int stackpos=0;
	public: 
		Frame(Method * m);
		Frame * newFrame(Method * m);
		Frame * newRefFrame(Method * m);
		Class * cl;
		int * opstack;
		long * opstackl;
		float * opstackf;
		double * opstackd;

		int * lv;
		long * lvl;
		float * lvf;
		double * lvd;

		//references
		int * lvpt;
		//program counter
		int pc;
		unsigned char * code;

		void push(int i);
		void pushl(long f);
		void pushf(float f);
		void pushd(double f);

		int pop();
		long popl();
		float popf();
		double popd();

		int get(int pos);
		int peek();
};
#endif
