#ifndef _CodeAttribute_h_
#define _CodeAttribute_h_

using namespace std; 

class Class;
class CodeAttribute : public Attribute
{ 
	private:
		CodeAttribute(unsigned short max_stack, unsigned short max_locals):max_stack(max_stack),max_locals(max_locals) {};
	public:
		const unsigned short max_stack;
		const unsigned short max_locals;
		MException ** exception_table;
		unsigned char * code;
		static CodeAttribute * read(String * name, Class * cl, ByteBuffer *  buf);
};
#endif
