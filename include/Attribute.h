#ifndef _Attribute_h_
#define _Attribute_h_

using namespace std; 

class Class;
class Attribute:public Object
{ 
	private:
    	String * name;
		static HashMap<String, Integer> * attr_map;
    public:
    	Attribute(){};
    	Attribute(String * name);
    	static Attribute * generate(Class * cl, ByteBuffer *  buf);
    	static void init();
};
#endif
