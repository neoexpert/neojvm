#ifndef _Executor_h_
#define _Executor_h_


class Executor:public Object
{ 
	private:
		FrameStack * stack;
		//current class
		Class * cC;
		Heap * heap;
	public: 
		Executor(Heap * heap, Method * m);
		int step();
};
#endif
