#ifndef _FrameStack_h_
#define _FrameStack_h_


class FrameStack:public Object
{ 
	private:
		Array<Frame> * stack;
		int stackpos=0;
    public: 
    	FrameStack();
    	void push(Frame * f);
    	Frame * pop();
    	Frame * peek();
    	bool isEmpty();
};
#endif
