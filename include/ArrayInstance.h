#ifndef _ArrayInstance_h_
#define _ArrayInstance_h_


class ArrayInstance:public Instance
{ 
	private:
		Class * cl;
	public:
		ArrayInstance(char type,int size);
		virtual Class * getClass(){return cl;}
};
#endif
