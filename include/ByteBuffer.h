#ifndef _ByteBuffer_h_
#define _ByteBuffer_h_
using namespace std; 

class ByteBuffer:public Object
{ 
	private:
		char * buffer;
	public:
		ByteBuffer(char * bp);
		void skip(int bytes);
		char get();
		short getShort();
		unsigned short getUShort();
		float getFloat();
		int getInt();
		double getDouble();
		long getLong();
};
#endif
