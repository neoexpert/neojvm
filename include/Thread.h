#ifndef _Thread_h_
#define _Thread_h_


class Thread:public Object
{ 
	private:
		String * name;
		Heap * heap;
		Executor * e;
	public:
		Thread(Method * m);
		void run();
};
#endif
