#include <string>
#include <iostream>

#include "lang/lang.h"

#include "include/ByteBuffer.h"
#include "include/Attribute.h"
#include "include/MException.h"
#include "include/CodeAttribute.h"
#include "include/Constant.h"
#include "include/Field.h"
#include "include/Method.h"
#include "include/Frame.h"
#include "include/Class.h"



using namespace std;

Attribute::Attribute(String * name){
	this->name=name;
}

//unordered_map<string, int> Attribute::attribute_map;
HashMap<String,Integer> * Attribute::attr_map=new HashMap<String,Integer>();
//static
void Attribute::init(){
	attr_map->put(_("Code"),_(1));
	attr_map->put(_("LineNumberTable"),_(2));
	attr_map->put(_("LocalVariableTable"),_(3));
	attr_map->put(_("StackMapTable"),_(4));
	attr_map->put(_("Exceptions"),_(5));
	attr_map->put(_("Signature"),_(6));
	attr_map->put(_("SourceFile"),_(7));
	//attribute_map["Code"] = 1;
}

//static
Attribute * Attribute::generate(Class * cl, ByteBuffer *  buf){
	unsigned short attribute_name_index = buf->getUShort();
	Constant * c = cl->getConstant(attribute_name_index);
	String * name=c->str;
	Integer * index=attr_map->get(name);
	if(index!=0){
		switch(index->value){
			case 1:
				return CodeAttribute::read(name, cl,  buf);
		}
	}
	int attribute_length = buf->getInt();
	//info = new byte[attribute_length];
	//buf.get(info);
	buf->skip(attribute_length);
	return new Attribute(name);
}
