#include <iostream>
#include <string>

#include "lang/lang.h"

#include "include/ByteBuffer.h"
#include "include/Attribute.h"
#include "include/Constant.h"
#include "include/Field.h"
#include "include/MException.h"
#include "include/CodeAttribute.h"
#include "include/Method.h"
#include "include/Frame.h"
#include "include/Class.h"
#include "include/Frame.h"
#include "include/FrameStack.h"
#include "include/Instance.h"
#include "include/Heap.h"
#include "include/Executor.h"
#include "include/Thread.h"
#include "include/Constant.h"
using namespace std;

void printUsage(char arr[]){
	cout<<"Usage: "<<arr<<" ClassFile"<<endl;
}

int main(int argc, char *argv[]){
	if(argc==1)
	{
		printUsage(argv[0]);
		return 0;
	}
	Attribute::init();
	String * classname=_(argv[1]);
	Class * c=Class::getClass(classname);


	Method * m=c->getMethod(_("main([Ljava/lang/String;)V"));
	if(m==0)
	{
		cout<<"no main method found"<<endl;
		return 1;
	}
	cout<<"main method found! we can init JVM now"<<endl;
	Thread * t= new Thread(m);
	t->run();
  return 0;
}
