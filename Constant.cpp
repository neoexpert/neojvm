#include<string>
#include<iostream>

#include "lang/lang.h"

#include"include/ByteBuffer.h"
#include"include/Constant.h"

using namespace std; 

		/*
	private String decodeString(ByteBuffer buf) {
		int size = buf.getChar(), oldLimit = buf.limit();
		buf.limit(buf.position() + size);
		StringBuilder sb = new StringBuilder(size + (size >> 1) + 16);
		while (buf.hasRemaining()) {
			byte b = buf.get();
			if (b > 0) sb.append((char) b);
			else {
				int b2 = buf.get();
				if ((b & 0xf0) != 0xe0)
					sb.append((char) ((b & 0x1F) << 6 | b2 & 0x3F));
				else {
					int b3 = buf.get();
					sb.append((char) ((b & 0x0F) << 12 | (b2 & 0x3F) << 6 | b3 & 0x3F));
				}
			}
		}
		buf.limit(oldLimit);
		return (sb.toString());
	}
	*/
		void Constant::decodeString(ByteBuffer * bb){
			unsigned short size=bb->getUShort();
			char * ca=new char[size+1];
			for(int i=0;i<size;i++)
				ca[i]=bb->get();
			ca[size]='\0';
			string s=ca;
			str = _(s);
			delete[] ca;
		}

Constant::Constant(ByteBuffer * bb){
   		tag=bb->get();
   		switch(tag){
			default:
				throw "unknown pool item type ";
				return;
			case CUtf8:
				decodeString(bb);

				//log(str);
    				std::cout << "utf8: "<<*str<<endl;
				return;
			case CClass:
			case CString:
			case CMethodType:
				//s = "%s ref=%d%n";
				index1 = bb->getUShort();
    				//std::cout << "CSomething"<<endl;
				break;
			case CFieldRef:
			case CMethodRef:
			case CInterfaceMethodRef:
			case CNameAndType:
    				//std::cout << "CSomething2"<<endl;
				//s = "%s ref1=%d, ref2=%d%n";
				index1 = bb->getUShort();
				index2 = bb->getUShort();
				break;
			case CInteger:
    				std::cout << "integer"<<endl;
				value = bb->getInt();
				//s = "%s value=" + ivalue + "%n";
				break;
			case CFloat:
    			cout << "float"<<endl;
				value = bb->getFloat();
    			cout << "floatvalue: "<<value<<endl;


				//s = "%s value=" + fvalue + "%n";
				break;
			case CDouble:
    				std::cout << "double"<<endl;
				lvalue = bb->getDouble();
				//s = "%s value=" + dvalue + "%n";
				break;
			case CLong:
    				std::cout << "long"<<endl;
				lvalue = bb->getLong();
				//s = "%s value=" + lvalue + "%n";
				break;
			case CMethodHandle:
    				std::cout << "CMethodHandle:"<<endl;
				//s = "%s kind=%d, ref=%d%n";
				index1 = bb->get();
				index2 = bb->getUShort();
				break;
			case CInvokeDynamic:
    				std::cout << "CInvokeDynamic"<<endl;
				//s = "%s bootstrap_method_attr_index=%d, ref=%d%n";
				index1 = bb->getUShort();
				index2 = bb->getUShort();
				break;
   		
   		}
    }
