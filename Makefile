jvm: *.cpp
	g++ -lzip *.cpp lang/lang/*.cpp lang/util/*.cpp -o jvm
debug: *.cpp
	g++ -lzip -g *.cpp lang/lang/*.cpp lang/util/*.cpp -o jvmdebug
printer:
	g++ -DBYTECODE_PRINTER *.cpp lang/lang/*.cpp lang/util/*.cpp -o jvm
debug_printer: *.cpp
	g++ -lzip -g -DBYTECODE_PRINTER *.cpp lang/lang/*.cpp lang/util/*.cpp -o jvmdebug
