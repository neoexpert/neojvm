#include<string>
#include<iostream>

#include "lang/lang.h"

#include"include/ByteBuffer.h"
#include"include/Constant.h"
#include"include/Attribute.h"
#include"include/Field.h"
#include"include/MException.h"
#include"include/CodeAttribute.h"
#include"include/Method.h"
#include"include/Frame.h"
#include"include/Class.h"


Frame::Frame(Method * m){
	this->m=m;
	code=m->codeattr->code;
	this->cl=m->cl;
	opstack=new int[m->getMaxStack()];
	opstackl=(long *)opstack;
	opstackf=(float *)opstack;
	opstackd=(double *)opstack;

	lv=new int[m->getMaxLV()];
	lvl=(long *)lv;
	lvf=(float *)lv;
	lvd=(double *)lv;

	lvpt=new int[m->getMaxLV()];
}

Frame * Frame::newFrame(Method * m){
	Frame * f = new Frame(m);
	f->code = m->codeattr->code;
	int p = m->getParamsArrayLength();
	ArrayList<Character> * args = m->args;
	/*
	if (f.lv == null) {
		f.lv = new Object[p];
		f.lvpt = new int[p];
	}
	*/
	for (int i = p - 1; i >= 0; i--) {
		Character * pt = args->get(i);
		if (*pt == 'J' || *pt == 'D') {
			f->lv[i - 1] = pop();
			f->lv[i] = pop();
			i--;
		} else
			f->lv[i] = pop();
		if(*pt=='L'||*pt=='[')
			f->lvpt[i]= (int) f->lv[i];

	}

	return f;
}

Frame * Frame::newRefFrame(Method * m){
	Frame * f = new Frame(m);
	f->code = m->codeattr->code;
	int p = m->getParamsArrayLength()+1;
	ArrayList<Character> * args = m->args;
	/*
	if (f.lv == null) {
		f.lv = new Object[p];
		f.lvpt = new int[p];
	}
	*/
	for (int i = p - 1; i >= 0; i--) {
		Character * pt = args->get(i);
		if (*pt == 'J' || *pt == 'D') {
			f->lv[i - 1] = pop();
			f->lv[i] = pop();
			i--;
		} else
			f->lv[i] = pop();
		if(*pt=='L'||*pt=='[')
			f->lvpt[i]= (int) f->lv[i];

	}

	return f;
}
void Frame::push(int i){
	opstack[stackpos++]=i;
}

void Frame::pushl(long l){
	opstackl[stackpos+=2]=l;
}

void Frame::pushf(float f){
	opstackf[stackpos++]=f;
}

void Frame::pushd(double d){
	opstackd[stackpos+=2]=d;
}

int Frame::pop(){
	return opstack[--stackpos];
}

int Frame::peek(){
	return opstack[stackpos-1];
}

int Frame::get(int pos){
	return opstack[pos];
}

long Frame::popl(){
	stackpos-=2;
	return opstackl[stackpos];
}

float Frame::popf(){
	return opstackf[--stackpos];
}

double Frame::popd(){
	stackpos-=2;
	return opstackd[stackpos];
}
