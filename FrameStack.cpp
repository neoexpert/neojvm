#include<string>
#include<iostream>

#include "lang/lang.h"

#include"include/ByteBuffer.h"
#include"include/Constant.h"
#include"include/Attribute.h"
#include"include/Field.h"
#include"include/MException.h"
#include"include/CodeAttribute.h"
#include"include/Method.h"
#include"include/Frame.h"
#include"include/Class.h"
#include"include/FrameStack.h"


FrameStack::FrameStack(){
	stack=new Array<Frame>(128);
}

void FrameStack::push(Frame * f){
	stack->set(stackpos,f);
	stackpos++;
}

Frame * FrameStack::pop(){
	return stack->get(--stackpos);
}

Frame * FrameStack::peek(){
	return stack->get(stackpos-1);
}

bool FrameStack::isEmpty(){
	return stackpos==0;
}
