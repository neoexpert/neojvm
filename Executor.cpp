#include<string>
#include<iostream>
#include <byteswap.h> 

#include "lang/lang.h"

#include"include/ByteBuffer.h"
#include"include/Constant.h"
#include"include/Attribute.h"
#include"include/MException.h"
#include"include/CodeAttribute.h"
#include"include/Field.h"
#include"include/Method.h"
#include"include/Frame.h"
#include"include/Class.h"
#include"include/FrameStack.h"
#include "include/Instance.h"
#include "include/ClassInstance.h"
#include "include/ArrayInstance.h"
#include "include/Heap.h"
#include"include/Executor.h"
#include"include/Heap.h"


Executor::Executor(Heap * heap, Method * m){
	stack=new FrameStack();
	Frame * f= new Frame(m);
	cC=f->cl;
	stack->push(f);
	this->heap=heap;
}

//The Holy Grail
int Executor::step() {
	if (stack->isEmpty())
	{
		//thread.destroy();
		return 1;
	}
	Frame * cF = stack->peek();
	//program counter
	int pc = cF->pc;
	//ClassInstance c;
	int i1, i2, i3, i4, aref, oref;
	long l1, l2;
	float f1, f2;
	double d1, d2;
	unsigned short * scode;
	int * icode;
	/*
	   AClass cl;
	   AMethod m;
	   ArrayInstance array;
	   Object obj;
	   Constant fc;
	   Constant fname;
	   Constant ftype;
	   */

	unsigned char * code = cF->code;
	int * lv=cF->lv;
	long * lvl=cF->lvl;
	float * lvf=cF->lvf;
	double * lvd=cF->lvd;
	//int steps = this.steps;
	mainLoop:;
	do {
		//System.out.println(stack.get(0).stackpos);

		switch (code[pc]) {
			case 0x00:
				//nop
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"nop"<<endl;
				#endif
				continue;
			case 0x01:
				//aconst_null
				cF->push(0);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"aconst_null"<<endl;
				#endif
				continue;
			case 0x02:
				//iconst_m1
				cF->push(-1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iconst_m1"<<endl;
				#endif
				continue;
			case 0x03:
				//iconst_0
				cF->push(0);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iconst_0"<<endl;
				#endif
				continue;
			case 0x04:
				//iconst_1
				cF->push(1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iconst_1"<<endl;
				#endif
				continue;
			case 0x05:
				//iconst_2
				cF->push(2);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iconst_2"<<endl;
				#endif
				continue;
			case 0x06:
				//iconst_3
				cF->push(3);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iconst_3"<<endl;
				#endif
				continue;
			case 0x07:
				//iconst_4
				cF->push(4);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iconst_4"<<endl;
				#endif
				continue;
			case 0x08:
				//iconst_5
				cF->push(5);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iconst_5"<<endl;
				#endif
				continue;
			case 0x09:
				//lconst_0
				cF->pushl(0l);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lconst_0"<<endl;
				#endif
				continue;
			case 0x0A:
				//lconst_1
				cF->pushl(0l);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lconst_1"<<endl;
				#endif
				continue;
			case 0x0B:
				//fconst_0
				cF->pushf(0.f);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fconst_0"<<endl;
				#endif
				continue;
			case 0x0C:
				//fconst_1
				cF->pushf(1.f);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fconst_1"<<endl;
				#endif
				continue;
			case 0x0D:
				//fconst_2
				cF->pushf(2.f);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fconst_2"<<endl;
				#endif
				continue;
			case 0x0E:
				//dconst_0
				cF->pushd(0.0);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dconst_0"<<endl;
				#endif
				continue;
			case 0x0F:
				//dconst_1
				cF->pushd(1.0);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dconst_1"<<endl;
				#endif
				continue;
			case 0x10:
				//bipush
				cF->push((int) code[pc + 1]);
				#ifdef BYTECODE_PRINTER
				cout<<"bipush "<<(int) code[pc + 1]<<endl;
				#endif
				pc += 2;
				continue;
			case 0x11:
				//sipush
				scode=(unsigned short *)(code+pc+1);
				cF->push(bswap_16(scode[0]));
				#ifdef BYTECODE_PRINTER
				cout<<"sipush: "<<bswap_16(scode[0])<<endl;
				#endif
				pc += 3;
				continue;
			case 0x12:
				//ldc
				cC->ldc((int) code[pc + 1] & 0xFF, cF);
				pc += 2;
				#ifdef BYTECODE_PRINTER
				cout<<"ldc "<<cF->peek()<<endl;
				#endif
				continue;
			case 0x13:
				//ldc_w
				scode=(unsigned short *)code;
				scode+=(pc+1);
				cC->ldc(scode[0], cF);
				pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"ldc_w"<<endl;
				#endif
				continue;
			case  0x14:
				//ldc2_w
				scode=(unsigned short *)code;
				scode+=(pc+1);
				cC->ldc2(scode[0], cF);
				pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"ldc2_w"<<endl;
				#endif
				continue;
			case 0x15:
				//iload
				cF->push(lv[(int) code[pc + 1]]);
				pc += 2;
				#ifdef BYTECODE_PRINTER
				cout<<"iload"<<endl;
				#endif
				continue;
			case 0x16:
				//lload
				cF->pushl(lvl[(int) code[pc + 1]]);
				pc += 2;
				#ifdef BYTECODE_PRINTER
				cout<<"lload"<<endl;
				#endif
				continue;
			case 0x17:
				//fload
				cF->pushf(lvf[(int) code[pc + 1]]);
				pc += 2;
				#ifdef BYTECODE_PRINTER
				cout<<"fload"<<endl;
				#endif
				continue;
			case 0x18:
				//dload
				cF->pushd(lvd[(int) code[pc + 1]]);
				pc += 2;
				#ifdef BYTECODE_PRINTER
				cout<<"dload"<<endl;
				#endif
				continue;
			case 0x19:
				//aload
				cF->push(lv[(int) code[pc + 1]]);
				pc += 2;
				#ifdef BYTECODE_PRINTER
				cout<<"aload"<<endl;
				#endif
				continue;
			case 0x1A:
				//iload_0
				cF->push(lv[0]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iload_0"<<endl;
				#endif
				continue;
			case 0x1B:
				//iload_1
				cF->push(lv[1]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iload_1"<<endl;
				#endif
				continue;
			case 0x1C:
				//iload_2
				cF->push(lv[2]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iload_2"<<endl;
				#endif
				continue;
			case 0x1D:
				//iload_3";
				cF->push(lv[3]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iload_3"<<endl;
				#endif
				continue;
			case 0x1E:
				//lload_0;
				cF->pushl(lvl[0]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lload_0"<<endl;
				#endif
				continue;
			case 0x1F:
				//lload_1
				cF->pushl(lvl[1]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lload_1"<<endl;
				#endif
				continue;
			case 0x20:
				//lload_2
				cF->pushl(lvl[2]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lload_2"<<endl;
				#endif
				continue;
			case 0x21:
				//lload_3
				cF->pushl(lvl[3]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lload_3"<<endl;
				#endif
				continue;
			case 0x22:
				//fload_0
				cF->pushf(lvf[0]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fload_0"<<endl;
				#endif
				continue;
			case 0x23:
				//fload_1
				cF->pushf(lvf[1]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fload_1"<<endl;
				#endif
				continue;
			case 0x24:
				//fload_2
				cF->pushf(lvf[2]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fload_2"<<endl;
				#endif
				continue;
			case 0x25:
				//fload_3;
				cF->pushf(lvf[3]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fload_3"<<endl;
				#endif
				continue;
			case 0x26:
				//dload_0;
				cF->pushd(lvd[0]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dload_0"<<endl;
				#endif
				continue;
			case 0x27:
				//dload_1
				cF->pushd(lvd[1]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dload_1"<<endl;
				#endif
				continue;
			case 0x28:
				//dload_2
				cF->pushd(lvd[2]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dload_2"<<endl;
				#endif
				continue;
			case 0x29:
				//dload_3
				cF->pushd(lvd[3]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dload_3"<<endl;
				#endif
				continue;
			case 0x2A:
				//aload_0
				cF->push(lv[0]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"aload_0"<<endl;
				#endif
				continue;
			case 0x2B:
				//aload_1
				cF->push(lv[1]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"aload_1"<<endl;
				#endif
				continue;
			case 0x2C:
				//aload_2
				cF->push(lv[2]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"aload_2"<<endl;
				#endif
				continue;
			case 0x2D:
				//aload_3
				cF->push(lv[3]);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"aload_3"<<endl;
				#endif
				continue;
			case 0x2E:
				//iaload
				i1 = (int) cF->pop();
				aref = (int) cF->pop();
				/*
				   array = (ArrayInstance) Heap.get(aref);
				   cF.push(((int[]) (array.a))[i1]);
				   */
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iaload"<<endl;
				#endif
				continue;
			case 0x2F:
				//laload
				i1 = (int) cF->pop();
				aref = (int) cF->pop();
				/*
				   array = (ArrayInstance) Heap.get(aref);
				   l1 = ((long[]) array.a)[i1];
				   cF.push(l2i1(l1));
				   cF.push(l2i2(l1));
				   */
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"laload"<<endl;
				#endif
				continue;
			case 0x30:
				//faload
				i1 = (int) cF->pop();
				aref = (int) cF->pop();
				/*
				   array = (ArrayInstance) Heap.get(aref);
				   cF.push(((float[]) array.a)[i1]);
				   */
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"faload"<<endl;
				#endif
				continue;
			case 0x31:
				//daload
				i1 = (int) cF->pop();
				aref = (int) cF->pop();
				/*
				   array = (ArrayInstance) Heap.get(aref);
				   cF.push(((double[]) array.a)[i1]);
				   cF.push(42);
				   cF.push(42);
				   */
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"daload"<<endl;
				#endif
				throw "double arrays not implemented";
				//continue;
			case 0x32:
				//aaload
				i1 = (int) cF->pop();
				aref = (int) cF->pop();
				/*
				   array = (ArrayInstance) Heap.get(aref);
				   cF.push(((int[]) array.a)[i1]);
				   */
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"aaload"<<endl;
				#endif
				continue;
			case  0x33:
				//baload
				i1 = (int) cF->pop();
				aref = (int) cF->pop();
				/*
				   array = (ArrayInstance) Heap.get(aref);
				   cF.push((int) ((byte[]) array.a)[i1]);
				   */
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"baload"<<endl;
				#endif
				continue;
			case 0x34:
				//caload
				i1 = (int) cF->pop();
				aref = (int) cF->pop();
				/*
				   array = (ArrayInstance) Heap.get(aref);
				   cF.push((int) ((char[]) array.a)[i1]);
				   */
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"caload"<<endl;
				#endif
				continue;
			case 0x35:
				//saload
				i1 = (int) cF->pop();
				aref = (int) cF->pop();
				/*
				   array = (ArrayInstance) Heap.get(aref);
				   cF.push((int) ((short[]) array.a)[i1]);
				   */
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"saload"<<endl;
				#endif
				continue;
			case 0x36:
				//istore
				lv[(int) code[pc + 1]] = (int) cF->pop();
				pc += 2;
				#ifdef BYTECODE_PRINTER
				cout<<"istore"<<endl;
				#endif
				continue;
			case 0x37:
				//lstore
				lvl[(int) code[pc + 1]] = cF->popl();
				pc += 2;
				#ifdef BYTECODE_PRINTER
				cout<<"lstore"<<endl;
				#endif
				continue;
			case 0x38:
				//fstore
				lvf[(int) code[pc + 1]] = cF->popf();
				pc += 2;
				#ifdef BYTECODE_PRINTER
				cout<<"fstore"<<endl;
				#endif
				continue;
			case 0x39:
				//dstore
				lvd[(int) code[pc + 1]] = cF->popd();
				pc += 2;
				#ifdef BYTECODE_PRINTER
				cout<<"dstore"<<endl;
				#endif
				continue;
			case 0x3A:
				//astore
				oref = (int) cF->pop();
				lv[code[pc + 1]] = oref;
				cF->lvpt[code[pc + 1]] = oref;
				pc += 2;
				#ifdef BYTECODE_PRINTER
				cout<<"astore"<<endl;
				#endif
				continue;
			case 0x3B:
				//istore_0
				lv[0] = cF->pop();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"istore_0"<<endl;
				#endif
				continue;
			case 0x3C:
				//istore_1
				lv[1] = cF->pop();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"istore_1"<<endl;
				#endif
				continue;
			case 0x3D:
				//istore_2
				lv[2] = cF->pop();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"istore_2"<<endl;
				#endif
				continue;
			case 0x3E:
				//istore_3
				lv[3] = cF->pop();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"istore_3"<<endl;
				#endif
				continue;
			case 0x3F:
				//lstore_0
				lvl[0] = cF->popl();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lstore_0"<<endl;
				#endif
				continue;
			case 0x40:
				//lstore_1
				lvl[1] = cF->popl();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lstore_1"<<endl;
				#endif
				continue;
			case 0x41:
				//lstore_2
				lvl[2] = cF->popl();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lstore_2"<<endl;
				#endif
				continue;
			case 0x42:
				//lstore_3
				lvl[3] = cF->popl();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lstore_3"<<endl;
				#endif
				continue;
			case 0x43:
				//fstore_0
				lvf[0] = cF->popf();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fstore_0"<<endl;
				#endif
				continue;
			case 0x44:
				//fstore_1
				lvf[1] = cF->popf();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fstore_1"<<endl;
				#endif
				continue;
			case 0x45:
				//fstore_2
				lvf[2] = cF->popf();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fstore_2"<<endl;
				#endif
				continue;
			case 0x46:
				//fstore_3"
				lvf[3] = cF->popf();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fstore_3"<<endl;
				#endif
				continue;
			case 0x47:
				//dstore_0
				lvd[0] = cF->popd();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dstore_0"<<endl;
				#endif
				continue;
			case 0x48:
				//dstore_1
				lvd[1] = cF->popd();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dstore_1"<<endl;
				#endif
				continue;
			case 0x49:
				//dstore_2
				lvd[2] = cF->popd();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dstore_2"<<endl;
				#endif
				continue;
			case 0x4A:
				//dstore_3
				lvd[3] = cF->popd();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dstore_3"<<endl;
				#endif
				continue;
			case 0x4B:
				//astore_0
				lv[0] = cF->pop();
				cF->lvpt[0]= (int) lv[0];
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"astore_0"<<endl;
				#endif
				continue;
			case 0x4C:
				//astore_1
				lv[1] = cF->pop();
				cF->lvpt[1]= lv[1];
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"astore_1"<<endl;
				#endif
				continue;
			case 0x4D:
				//astore_2
				lv[2] = cF->pop();
				cF->lvpt[2] = lv[2];
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"astore_2"<<endl;
				#endif
				continue;
			case 0x4E:
				//astore_3
				lv[3] = cF->pop();
				cF->lvpt[3]= lv[3];
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"astore_3"<<endl;
				#endif
				continue;
			case 0x4F:
				//iastore
				i1 = (int) cF->pop();
				i2 = (int) cF->pop();
				aref = (int) cF->pop();
				/*
				array = (ArrayInstance) Heap.get(aref);
				((int[]) array.a)[i2] = i1;
				*/
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iastore"<<endl;
				#endif
				continue;
			case 0x50:
				//lastore
				l1 = cF->popl();
				i3 = (int) cF->pop();
				aref = (int) cF->pop();
				/*
				array = (ArrayInstance) Heap.get(aref);
				((long[]) array.a)[i3] = l1;
				*/
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lastore"<<endl;
				#endif
				continue;
			case 0x51:
				//fastore
				f1 = cF->popf();
				i1 = cF->popf();
				aref = cF->pop();
				/*
				array = (ArrayInstance) Heap.get(aref);
				((float[]) array.a)[i1] = f1;
				*/
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fastore"<<endl;
				#endif
				continue;
			case 0x52:
				//dastore
				aref = (int) cF->pop();
				i1 = (int) cF->pop();
				i2 = (int) cF->pop();
				i3 = (int) cF->pop();
				pc += 1;
				throw "implement me!";
				#ifdef BYTECODE_PRINTER
				cout<<"dastore"<<endl;
				#endif
				//continue;
			case 0x53:
				//aastore
				i1 = cF->pop();
				i2 = cF->pop();
				aref = cF->pop();
				/*
				array = (ArrayInstance) Heap.get(aref);
				((int[]) array.a)[i2] = i1;
				*/
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"aastore"<<endl;
				#endif
				continue;
			case 0x54:
				//bastore
				i1 = (int) cF->pop();
				i2 = (int) cF->pop();
				aref = (int) cF->pop();
				/*
				array = (ArrayInstance) Heap.get(aref);
				((byte[]) array.a)[i2] = (byte) i1;
				*/
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"bastore"<<endl;
				#endif
				continue;
			case 0x55:
				//castore
				i1 = cF->pop();
				i2 = cF->pop();
				aref = cF->pop();
				/*
				array = (ArrayInstance) Heap.get(aref);
				((char[]) array.a)[i2] = (char) i1;
				*/
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"castore"<<endl;
				#endif
				continue;
			case 0x56:
				//sastore
				i1 = cF->pop();
				i2 = cF->pop();
				aref = cF->pop();
				/*
				array = (ArrayInstance) Heap.get(aref);
				((short[]) array.a)[i2] = (short) i1;
				*/
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"sastore"<<endl;
				#endif
				continue;
			case 0x57:
				//pop
				cF->pop();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"pop"<<endl;
				#endif
				continue;
			case 0x58:
				//pop2
				cF->pop();
				cF->pop();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"pop2"<<endl;
				#endif
				continue;
			case 0x59:
				//dup
				cF->push(cF->peek());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dup"<<endl;
				#endif
				continue;
			case 0x5A:
				//dup_x1
				i1 = cF->pop();
				i2 = cF->pop();
				cF->push(i1);
				cF->push(i2);
				cF->push(i1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dup_x1"<<endl;
				#endif
				continue;
			case 0x5B:
				//dup_x2
				i1 = cF->pop();
				i2 = cF->pop();
				i3 = cF->pop();
				cF->push(i1);
				cF->push(i3);
				cF->push(i2);
				cF->push(i1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dup_x2"<<endl;
				#endif
				continue;
			case 0x5C:
				//dup2
				i2 = cF->pop();
				i1 = cF->pop();
				cF->push(i1);
				cF->push(i2);
				cF->push(i1);
				cF->push(i2);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dup2"<<endl;
				#endif
				continue;
			case 0x5D:
				//dup2_x1
				i2 = cF->pop();
				i1 = cF->pop();
				i3 = cF->pop();
				cF->push(i1);
				cF->push(i2);
				cF->push(i3);
				cF->push(i1);
				cF->push(i2);
				pc += 1;
				throw "check that and contniue";
				#ifdef BYTECODE_PRINTER
				cout<<"dup2_x1"<<endl;
				#endif
				continue;
			case 0x5E:
				//dup2_x2
				i2 = cF->pop();
				i1 = cF->pop();
				i4 = cF->pop();
				i3 = cF->pop();
				cF->push(i1);
				cF->push(i2);
				cF->push(i3);
				cF->push(i4);
				cF->push(i1);
				cF->push(i2);
				pc += 1;
				throw "check that and continue";
				#ifdef BYTECODE_PRINTER
				cout<<"dup2_x2"<<endl;
				#endif
				continue;
			case 0x5F:
				//swap
				cF->push(cF->get(0));
				pc += 1;
				throw "check that and continue";
				#ifdef BYTECODE_PRINTER
				cout<<"swap"<<endl;
				#endif
				continue;
			case 0x60:
				//iadd
				i1 = cF->pop();
				i2 = cF->pop();
				cF->push(i1 + i2);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iadd"<<endl;
				#endif
				continue;
			case 0x61:
				//ladd
				l1 = cF->popl();
				l2 = cF->popl();
				cF->pushl(l2+l1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"ladd"<<endl;
				#endif
				continue;
			case 0x62:
				//fadd
				f1 = cF->popf();
				f2 = cF->popf();
				cF->push(f1 + f2);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fadd"<<endl;
				#endif
				continue;
			case 0x63:
				//dadd
				d1 = cF->popd();
				d2 = cF->popd();
				cF->pushd(d2+d1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dadd"<<endl;
				#endif
				continue;
			case 0x64:
				//isub
				i1 = cF->pop();
				i2 = cF->pop();
				cF->push(i2 - i1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"isub"<<endl;
				#endif
				continue;
			case 0x65:
				//lsub
				l1 = cF->popl();
				l2 = cF->popl();
				cF->pushl(l2-l1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lsub"<<endl;
				#endif
				continue;
			case 0x66:
				//fsub
				f1 = cF->popf();
				f2 = cF->popf();
				cF->pushf(f2 - f1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fsub"<<endl;
				#endif
				continue;
			case 0x67:
				//dsub
				d1 = cF->popd();
				d2 = cF->popd();
				cF->pushd(d2-d1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dsub"<<endl;
				#endif
				continue;
			case 0x68:
				//imul
				i1 = cF->pop();
				i2 = cF->pop();
				cF->push(i2 * i1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"imul"<<endl;
				#endif
				continue;
			case 0x69:
				//lmul
				l1 = cF->popl();
				l2 = cF->popl();
				cF->pushl(l2*l1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lmul"<<endl;
				#endif
				continue;
			case 0x6A:
				//fmul
				f1 = cF->popf();
				f2 = cF->popf();
				cF->pushf(f2 * f1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fmul"<<endl;
				#endif
				continue;
			case 0x6B:
				//dmul
				d1 = cF->popd();
				d2 = cF->popd();
				cF->pushd(d2*d1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dmul"<<endl;
				#endif
				continue;
			case 0x6C:
				//idiv
				i1 = cF->pop();
				i2 = cF->pop();
				cF->push(i2 / i1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"idiv"<<endl;
				#endif
				continue;
			case 0x6D:
				//ldiv
				l1 = cF->popl();
				l2 = cF->popl();
				cF->pushl(l2/l1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"ldiv"<<endl;
				#endif
				continue;
			case 0x6E:
				//fdiv
				f1 = cF->popf();
				f2 = cF->popf();
				cF->pushf(f2 / f1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fdiv"<<endl;
				#endif
				continue;
			case 0x6F:
				//ddiv
				d1 = cF->popd();
				d2 = cF->popd();
				cF->pushd(d2/d1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"ddiv"<<endl;
				#endif
				continue;
			case 0x70:
				//irem
				i1 = cF->pop();
				i2 = cF->pop();
				cF->push(i2 % i1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"irem"<<endl;
				#endif
				continue;
			case 0x71:
				//lrem
				l1 = cF->popl();
				l2 = cF->popl();
				cF->pushf(l2%l1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lrem"<<endl;
				#endif
				continue;
			case 0x72:
				//frem
				f1 = cF->popf();
				f2 = cF->popf();
				//cF->pushf(f2 % f1);
				pc += 1;
				throw "implemet me";
				#ifdef BYTECODE_PRINTER
				cout<<"frem"<<endl;
				#endif
				continue;
			case 0x73:
				//drem
				d1 = cF->popd();
				d2 = cF->popd();
				//cF->pushd(d2%d1);
				pc += 1;
				throw "implemet me";
				#ifdef BYTECODE_PRINTER
				cout<<"drem"<<endl;
				#endif
				continue;
			case 0x74:
				//ineg
				cF->push(- cF->pop());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"ineg"<<endl;
				#endif
				continue;
			case 0x75:
				//lneg
				l1 = cF->popl();
				cF->pushl(-l1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lneg"<<endl;
				#endif
				continue;
			case 0x76:
				//fneg
				cF->pushf(-cF->popf());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fneg"<<endl;
				#endif
				continue;
			case 0x77:
				//dneg
				cF->pushd(cF->popd());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dneg"<<endl;
				#endif
				continue;
			case 0x78:
				//ishl
				i1 = cF->pop();
				i2 = cF->pop();
				cF->push(i2 << i1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"ishl"<<endl;
				#endif
				continue;
			case 0x79:
				//lshl
				i1 = cF->pop();
				l1 = cF->popl() << i1;
				cF->pushl(l1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lshl"<<endl;
				#endif
				continue;
			case 0x7A:
				//ishr
				i1 = cF->pop();
				i2 = cF->pop();
				cF->push(i2 >> i1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"ishr"<<endl;
				#endif
				continue;
			case 0x7B:
				//lshr
				i1 = cF->pop();
				l2 = cF->popl();
				cF->pushl(l2 >> i1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lshr"<<endl;
				#endif
				continue;
			case 0x7C:
				//iushr
				i1 = cF->pop();
				i2 = cF->pop();
				//cF->push(i2 >>> i1);
				pc += 1;
				throw "implement me";
				#ifdef BYTECODE_PRINTER
				cout<<"iushr"<<endl;
				#endif
				continue;
			case 0x7D:
				//lushr
				i1 = cF->pop();
				l2 = cF->popl();
				//cF->pushl(l2 >>> i1);
				pc += 1;
				throw "implement me";
				#ifdef BYTECODE_PRINTER
				cout<<"lushr"<<endl;
				#endif
				continue;
			case 0x7E:
				//iand
				i1 = cF->pop();
				i2 = cF->pop();
				cF->push(i2 & i1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"iand"<<endl;
				#endif
				continue;
			case 0x7F:
				//land
				l1 = cF->popl();
				l2 = cF->popl();
				cF->pushl(l2&l1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"land"<<endl;
				#endif
				continue;
			case 0x80:
				//ior
				i1 = cF->pop();
				i2 = cF->pop();
				cF->push(i2 | i1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"ior"<<endl;
				#endif
				continue;
			case 0x81:
				//lor
				l1 = cF->popl();
				l2 = cF->popl();
				cF->pushl(l2|l1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lor"<<endl;
				#endif
				continue;
			case 0x82:
				//ixor
				i1 = cF->pop();
				i2 = cF->pop();
				cF->push(i2 ^ i1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"ixor"<<endl;
				#endif
				continue;
			case 0x83:
				//lxor
				l1 = cF->popl();
				l2 = cF->popl();
				cF->pushl(l2^l1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lxor"<<endl;
				#endif
				continue;
			case 0x84:
				//iinc
				i1 = lv[(int) code[pc + 1]];
				lv[(int) code[pc + 1]] = i1 + code[pc + 2];
				pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"iinc"<<endl;
				#endif
				continue;
			case 0x85:
				//i2l
				cF->pushl((long)cF->pop());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"i2l"<<endl;
				#endif
				continue;
			case 0x86:
				//i2f
				cF->pushf((float)cF->pop());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"i2f"<<endl;
				#endif
				continue;
			case 0x87:
				//i2d
				cF->pushd((double)cF->pop());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"i2d"<<endl;
				#endif
				continue;
			case 0x88:
				//l2i
				cF->push((int)cF->popl());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"l2i"<<endl;
				#endif
				continue;
			case 0x89:
				//l2f
				cF->pushf((float)cF->popl());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"l2f"<<endl;
				#endif
				continue;
			case 0x8A:
				//l2d
				cF->pushd((double)cF->popl());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"l2d"<<endl;
				#endif
				continue;
			case 0x8B:
				//f2i
				cF->push((int)cF->popf());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"f2i"<<endl;
				#endif
				continue;
			case 0x8C:
				//f2l
				cF->pushl((long)cF->popf());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"f2l"<<endl;
				#endif
				continue;
			case 0x8D:
				//f2d
				cF->pushd((double)cF->popf());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"f2d"<<endl;
				#endif
				continue;
			case 0x8E:
				//d2i
				cF->push((int)cF->popd());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"d2i"<<endl;
				#endif
				continue;
			case 0x8F:
				//d2l
				cF->pushl((long)cF->popd());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"d2l"<<endl;
				#endif
				continue;
			case 0x90:
				//d2f
				cF->pushf((float)cF->popd());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"d2f"<<endl;
				#endif
				continue;
			case 0x91:
				//i2b
				cF->push((int)(char)cF->pop());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"i2b"<<endl;
				#endif
				continue;
			case 0x92:
				//i2c
				cF->push((int)(unsigned short)cF->pop());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"i2c"<<endl;
				#endif
				continue;
			case 0x93:
				//i2s
				cF->push((int)(short)cF->pop());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"i2s"<<endl;
				#endif
				continue;
			case 0x94:
				//lcmp
				l1 = cF->popl();
				l2 = cF->popl();
				if (l2 == l1) {
					cF->push(0);
				} else if (l2 > l1) {
					cF->push(1);
				} else
					cF->push(-1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lcmp"<<endl;
				#endif
				continue;
			case 0x95:
				//fcmpl
				f1 = cF->popf();
				f2 = cF->popf();
				if (f2 == f1) {
					cF->push(0);
				} else if (f2 > f1) {
					cF->push(1);
				} else if (f2 < f1)
					cF->push(-1);
				else
					cF->push(-1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fcmpl"<<endl;
				#endif
				continue;
			case 0x96:
				//fcmpg
				f1 = cF->popf();
				f2 = cF->popf();
				if (f2 == f1) {
					cF->push(0);
				} else if (f2 > f1) {
					cF->push(1);
				} else if (f2 < f1)
					cF->push(-1);
				else
					cF->push(1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"fcmpg"<<endl;
				#endif
				continue;
			case 0x97:
				//dcmpl
				d1 = cF->popd();
				d2 = cF->popd();
				if (d2 == d1) {
					cF->push(0);
				} else if (d2 > d1) {
					cF->push(1);
				} else if (d2 < d1)
					cF->push(-1);
				else
					cF->push(-1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dcmpl"<<endl;
				#endif
				continue;
			case 0x98:
				//dcmpg
				d1 = cF->popd();
				d2 = cF->popd();
				if (d2 == d1) {
					cF->push(0);
				} else if (d2 > d1) {
					cF->push(1);
				} else if (d2 < d1)
					cF->push(-1);
				else
					cF->push(1);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dcmpg"<<endl;
				#endif
				continue;
			case 0x99:
				//ifeq
				scode=(unsigned short *)(code+pc+1);
				if (cF->pop() == 0)
					pc += bswap_16(scode[0]);
					//pc += i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"ifeq"<<endl;
				#endif
				continue;
			case 0x9A:
				//ifne
				scode=(unsigned short *)(code+pc+1);
				scode=(unsigned short *)code;
				scode+=(pc+1);
				if ((int) cF->pop() != 0)
					pc += bswap_16(scode[0]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"ifne"<<endl;
				#endif
				continue;
			case 0x9B:
				//iflt
				scode=(unsigned short *)(code+pc+1);
				scode=(unsigned short *)code;
				scode+=(pc+1);
				if ((int) cF->pop() < 0)
					pc += bswap_16(scode[0]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"iflt"<<endl;
				#endif
				continue;
			case 0x9C:
				//ifge
				scode=(unsigned short *)(code+pc+1);
				scode=(unsigned short *)code;
				scode+=(pc+1);
				if ((int) cF->pop() >= 0)
					pc += bswap_16(scode[0]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"ifge"<<endl;
				#endif
				continue;
			case 0x9D:
				//ifgt
				scode=(unsigned short *)(code+pc+1);
				scode=(unsigned short *)code;
				scode+=(pc+1);
				if ((int) cF->pop() > 0)
					pc += bswap_16(scode[0]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"ifgt"<<endl;
				#endif
				continue;
			case 0x9E:
				//ifle
				scode=(unsigned short *)(code+pc+1);
				scode=(unsigned short *)code;
				scode+=(pc+1);
				if ((int) cF->pop() <= 0)
					pc += bswap_16(scode[0]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"ifle"<<endl;
				#endif
				continue;
			case 0x9F:
				//if_icmpeq
				scode=(unsigned short *)(code+pc+1);
				scode=(unsigned short *)code;
				scode+=(pc+1);
				if ((int) cF->pop() == (int) cF->pop())
					pc += bswap_16(scode[0]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"if_icmpeq"<<endl;
				#endif
				continue;
			case 0xA0:
				//if_icmpne
				scode=(unsigned short *)(code+pc+1);
				scode=(unsigned short *)code;
				scode+=(pc+1);
				if ((int) cF->pop() != (int) cF->pop())
					pc += bswap_16(scode[0]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"if_icmpne"<<endl;
				#endif
				continue;
			case 0xA1:
				//if_icmplt
				scode=(unsigned short *)(code+pc+1);
				scode=(unsigned short *)code;
				scode+=(pc+1);
				if ((int) cF->pop() > (int) cF->pop())
					pc += bswap_16(scode[0]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"if_icmplt"<<endl;
				#endif
				continue;
			case 0xA2:
				//if_icmpge
				scode=(unsigned short *)(code+pc+1);
				scode=(unsigned short *)code;
				scode+=(pc+1);
				if ((int) cF->pop() <= (int) cF->pop())
					pc += bswap_16(scode[0]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"if_icmpge"<<endl;
				#endif
				continue;
			case 0xA3:
				//if_icmpgt
				scode=(unsigned short *)(code+pc+1);
				scode=(unsigned short *)code;
				scode+=(pc+1);
				if ((int) cF->pop() < (int) cF->pop())
					pc += bswap_16(scode[0]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"if_icmpgt"<<endl;
				#endif
				continue;
			case 0xA4:
				//if_icmple
				scode=(unsigned short *)(code+pc+1);
				if ((int) cF->pop() >= (int) cF->pop())
					pc += bswap_16(scode[0]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"if_icmple"<<endl;
				#endif
				continue;
			case 0xA5:
				//if_acmpeq
				scode=(unsigned short *)(code+pc+1);
				scode=(unsigned short *)code;
				scode+=(pc+1);
				if ((int) cF->pop() == (int) cF->pop())
					pc += bswap_16(scode[0]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"if_acmpeq"<<endl;
				#endif
				continue;
			case 0xA6:
				//if_acmpeq
				scode=(unsigned short *)(code+pc+1);
				scode=(unsigned short *)code;
				scode+=(pc+1);
				if ((int) cF->pop() != (int) cF->pop())
					pc += bswap_16(scode[0]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"if_acmpeq"<<endl;
				#endif
				continue;
			case 0xA7:
				//goto
				scode=(unsigned short *)(code+pc+1);
				pc += bswap_16(scode[0]);
				//pc += i(code[pc + 1], code[pc + 2]);
				#ifdef BYTECODE_PRINTER
				cout<<"goto "<<bswap_16(scode[0])<<endl;
				#endif
				continue;
			case 0xA8:
				//jsr
				cF->push(pc + 3);
				scode=(unsigned short *)code;
				scode+=(pc+1);
				pc += scode[0];
				#ifdef BYTECODE_PRINTER
				cout<<"jsr"<<endl;
				#endif
				continue;
			case 0xA9:
				//ret
				pc = (int) lv[code[pc + 1]];
				#ifdef BYTECODE_PRINTER
				cout<<"ret"<<endl;
				#endif
				continue;
			case  0xAA:
				{
					//tableswitch
					i1 = cF->pop();
					int pc2 = pc + (4 - pc % 4);

					icode=(int *)code;
					icode+=(pc2);

					int default_offset = icode[0];
					pc2 += 4;
					int low = icode[1];
					pc2 += 4;
					int high = icode[2];
					pc2 += 4;
					if (i1 < low || i1 > high) {  // if its less than <low> or greater than <high>,
						pc += default_offset;
						// branch to default
					} else {
						pc2 += (i1 - low) * 4;
						icode=(int *)code;
						icode+=(pc2);
						pc += icode[0];
					}
				}
				#ifdef BYTECODE_PRINTER
				cout<<"tableswitch"<<endl;
				#endif
				continue;
			case 0xAB:
				{
					//lookupswitch
					i1 = cF->pop();
					int pc2 = pc + (4 - pc % 4);

					icode=(int *)code;
					icode+=(pc2);

					int default_offset = icode[0];
					icode+=4;
					//pc2 += 4;
					i2 = icode[1];
					icode+=4;
					//pc2 += 4;
					for (i3 = 0; i3 < i2; i3++) {
						int key = icode[0];

						if (key == i1) {
							icode += 4;
							int value = icode[0];
							pc += value;
							//the only way to continue the outer loop:
							goto mainLoop;
						}
						icode += 8;
					}
					pc += default_offset;
				}
				#ifdef BYTECODE_PRINTER
				cout<<"lookupswitch"<<endl;
				#endif
				continue;
			case 0xAC:
				//ireturn
				i1 = cF->pop();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"ireturn"<<endl;
				#endif
				//last_frame = stack.pop();
				stack->pop();
				if (stack->isEmpty()) {
					//last_frame.pc=pc;
					//thread.destroy();
					return 1;
				}
				cF = stack->peek();
				pc = cF->pc;
				lv = cF->lv;
				cC = cF->cl;
				//System.out.println("ireturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
				cF->push(i1);
				code = (unsigned char *) cF->code;
				// this.code=c.getMethod(11).attrs[0].code;
				continue;
			case 0xAD:
				//lreturn
				l1 = cF->popl();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"lreturn"<<endl;
				#endif
				//last_frame = stack.pop();
				stack->pop();
				if (stack->isEmpty()) {
					//last_frame.pc=pc;
					//thread.destroy();
					return 1;
				}
				cF = stack->peek();
				pc = cF->pc;
				lv = cF->lv;
				cC = cF->cl;
				cF->pushl(l1);
				code = cF->code;
				//System.out.println("lreturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
				continue;
			case  0xAE:
				//freturn
				f1 = cF->popf();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"freturn"<<endl;
				#endif
				//last_frame = stack.pop();
				stack->pop();
				if (stack->isEmpty()) {
					//last_frame.pc=pc;
					//thread.destroy();
					return 1;
				}
				cF = stack->peek();
				pc = cF->pc;
				lv = cF->lv;
				cC = cF->cl;
				//System.out.println("freturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
				cF->pushf(f1);
				code = cF->code;
				// this.code=c.getMethod(11).attrs[0].code;
				continue;
			case 0xAF:
				//dreturn
				d1 = cF->popd();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"dreturn"<<endl;
				#endif
				//last_frame = stack.pop();
				stack->pop();
				if (stack->isEmpty()) {
					//last_frame.pc=pc;
					//thread.destroy();
					return 1;
				}
				cF = stack->peek();
				pc = cF->pc;
				lv = cF->lv;
				cC = cF->cl;
				//System.out.println("dreturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
				cF->pushd(d1);
				code = cF->code;
				// this.code=c.getMethod(11).attrs[0].code;
				continue;
			case 0xB0:
				//areturn
				i1 = cF->pop();
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"areturn"<<endl;
				#endif
				//last_frame = stack.pop();
				stack->pop();
				if (stack->isEmpty()) {
					//last_frame.pc=pc;
					//thread.destroy();
					return 1;
				}
				cF = stack->peek();
				pc = cF->pc;
				lv = cF->lv;
				cC = cF->cl;
				//System.out.println("areturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
				cF->push(i1);
				code = cF->code;
				// this.code=c.getMethod(11).attrs[0].code;
				continue;
			case 0xB1:
				//_return
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"return"<<endl;
				#endif
				//last_frame = stack.pop();
				stack->pop();
				if (stack->isEmpty()) {
					//last_frame.pc=pc;
					//thread.destroy();
					return 1;
				}
				cF = stack->peek();
				pc = cF->pc;
				lv = cF->lv;
				cC = cF->cl;
				code = cF->code;
				//System.out.println("return: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
				continue;
			case 0xB2:
				//getstatic
				scode=(unsigned short *)code;
				scode+=(pc+1);
				cC->getStatic(scode[0], cF);
				pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"getstatic"<<endl;
				#endif
				continue;
			case 0xB3:
				//putstatic
				/*
				obj = cF.pop();
				fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
				fc = cC.getConstant(fc.index2);
				fname = cC.getConstant(fc.index1);
				ftype = cC.getConstant(fc.index2);
				switch (ftype.str) {
					case "J":
						obj = ((Integer) cF.pop()).longValue() << 32 | ((Integer) (obj)).longValue() & 0xFFFFFFFFL;
						break;
					case "D":
						l1 = ((Integer) cF.pop()).longValue() << 32 | ((Integer) (obj)).longValue() & 0xFFFFFFFFL;
						obj = Double.longBitsToDouble(l1);

						//throw new RuntimeException("putfield error");
						break;
				}
				// int addrr=(code[i+1] << 8) | code[i+2];
				cC.putStatic(fname.str, obj);
				*/
				pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"putstatic"<<endl;
				#endif
				continue;
			case 0xB4:
				//getfield
				/*
				oref = (int) cF.pop();
				obj = Heap.get(oref);
				fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
				fc = cC.getConstant(fc.index2);
				fname = cC.getConstant(fc.index1);
				ftype = cC.getConstant(fc.index2);
				if (obj != null)
					obj = ((ClassInstance) obj).getField(fname.str);
				else obj = 0;
				switch (ftype.str) {
					case "J":
						cF.push(l2i2((Long) obj));
						cF.push(l2i1((Long) obj));
						break;
					case "D":
						cF.push(l2i2((Long) obj));
						cF.push(l2i1((Long) obj));
						//throw new RuntimeException("getfield error");
						break;
					default:
						cF.push(obj);
						break;
				}
				*/
				pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"getfield"<<endl;
				#endif
				continue;
			case 0xB5:
				//putfield
				/*
				obj = cF.pop();
				fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
				fc = cC.getConstant(fc.index2);
				fname = cC.getConstant(fc.index1);
				ftype = cC.getConstant(fc.index2);
				switch (ftype.str) {
					case "J":
						obj = ((Integer) cF.pop()).longValue() << 32 | ((Integer) (obj)).longValue() & 0xFFFFFFFFL;
						break;
					case "D":
						obj = ((Integer) cF.pop()).longValue() << 32 | ((Integer) (obj)).longValue() & 0xFFFFFFFFL;
						break;
				}
				Object o = cF.pop();
				oref = (int) o;
				c = (ClassInstance) Heap.get(oref);
				if (c == null)
					throw new NullPointerException("field " + fname.str + " reference: " + oref );

				c.putField(fname.str, obj);
				*/
				pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"putfield"<<endl;
				#endif
				continue;

				/*
			case 0xB6:
				//invokevirtual
				// int objref=(int)cF.pop();

				m = cC.getMethod(ii(code[pc + 1], code[pc + 2]));
				pc += 3;
				cF.pc = pc;
				//System.out.println("invokevirtual: " + m.getMyClass().getName() + "." + m.getSignature());
				//Frame nf = cF.newRefFrame(m);
				Frame nf = cF.newRefFrame((Method)m);
				oref = (int) nf.lv[0];
				//if (m.isAbstract()) 
				String mname = m.getSignature();
				cl=null;
				try {

					cl = ((Instance) Heap.get(oref)).getMyClass();
				}
				catch (NullPointerException e){
					getClass();
				}
				m = cl.getMethod(mname);
				if (m.isNative()) {
					if(Natives.invoke(m.getMyClass().getName(), m.getSignature(), cF, nf.lv,thread))
						return 1;
					cF = stack.peek();
					if(cF.code_type!= Method.BYTE_CODE)
						return cF.code_type;
					pc = cF.pc;
					code = (byte[]) cF.code;
					lv = cF.lv;

				#ifdef BYTECODE_PRINTER
				cout<<"invokevirtual"<<endl;
				#endif
					continue;
				}
				if (m == null)
					throw new RuntimeException("method " + cl.getName() + "." + mname + " not found");
				nf.initLocals((Method) m);
				nf.setMethod((Method) m);

				//nf.code = m.getCode();
				cF = nf;
				pc = cF.pc;
				lv = cF.lv;

				cC = m.getMyClass();
				if (classa != null)
					classa.setFrame(cF);
				stack.push(cF);
				if(cF.code_type!= Method.BYTE_CODE)
					return cF.code_type;
				code = (byte[]) cF.code;
				//cF.code = m.getCode();
				#ifdef BYTECODE_PRINTER
				cout<<"invokevirtual"<<endl;
				#endif
				continue;
				*/
			case 0xB7:
				//invokespecial
				{
					scode=(unsigned short *)(code+pc+1);
					Method * m = cC->resolveMethod(bswap_16(scode[0]));
					pc += 3;
					cF->pc = pc;
					//System.out.println("invokespecial: " + m.getMyClass().getName() + "." + m.getSignature());

					Frame * nf = cF->newRefFrame(m);
					//Heap.objectCreated((int) nf.lv[0]);
					if (m->isNative()) {
						/*
						// m.invoke(cF.stack, this);
						if(Natives.invoke(m.getMyClass().getName(), m.getSignature(), cF, nf.lv,thread))
							return 1;
						cF = stack.peek();
						if(cF.code_type!= Method.BYTE_CODE)
							return cF.code_type;
						pc = cF.pc;
						code = (byte[]) cF.code;
						lv = cF.lv;
						*/
#ifdef BYTECODE_PRINTER
						cout<<"invokespecial"<<endl;
#endif
						continue;
					}
					cF = nf;
					pc = cF->pc;
					lv = cF->lv;
					cC = m->getClass();
					stack->push(cF);
					code = cF->code;
#ifdef BYTECODE_PRINTER
					cout<<"invokespecial"<<endl;
#endif
				}
				continue;
			case 0xB8:
				//invokestatic
				{
					scode=(unsigned short *)(code+pc+1);
					Method * m = cC->resolveMethod(bswap_16(scode[0]));
					pc += 3;
					cF->pc = pc;

					//System.out.println("invokestatic: " + m.getMyClass().getName() + "." + m.getSignature());

					Frame * nf = cF->newFrame(m);
					if (m->isNative()) {
						/*
						   if(Natives.invoke(m.getMyClass().getName(), m.getSignature(), cF, nf.lv,thread))
						   return 1;
						   cF = stack.peek();
						   if(cF.code_type!= Method.BYTE_CODE)
						   return cF.code_type;
						   pc = cF.pc;
						   code = (byte[]) cF.code;
						   lv = cF.lv;
						   */
#ifdef BYTECODE_PRINTER
						cout<<"invokestatic "<<*m->getSignature()<<endl;
#endif
						continue;
					}
					cF = nf;
					pc = cF->pc;
					code = cF->code;
					lv = cF->lv;
					cC = m->getClass();
					stack->push(cF);
#ifdef BYTECODE_PRINTER
					cout<<"invokestatic "<<*m->getSignature()<<endl;
#endif
				}
				continue;
				/*
			case 0xB9:
				//invokeinterface
				m = cC.getMethod(ii(code[pc + 1], code[pc + 2]));
				//System.out.println("invokeinterface: " + m.getMyClass().getName() + "." + m.getSignature());

				byte count = code[pc + 3];
				byte zero = code[pc + 4];
				pc += 5;
				cF.pc = pc;
				//if(count>1) {
				nf = new Frame((Method) m);

				Object[] mlv = new Object[count];
				for (int ix = 0; ix < count; ix++) {
					//nf.getLv().put(p - ix - 1, cF.pop());
					mlv[count - ix - 1] = cF.pop();
				}

				//}
				oref = (int) mlv[0];

				ClassInstance ci = (ClassInstance) Heap.get(oref);
				AClass mc = ci.getMyClass();
				m = mc.getMethod(m.getSignature());


				if (m.isNative()) {
					//m.invoke(cF.stack, this);
					throw new RuntimeException("check.that");
					//continue;
				}
				nf.lv = new Object[((Method)m).getMaxLocals()];
				nf.lvpt = new int[((Method)m).getMaxLocals()];

				for (int j = 0; j < mlv.length; j++) {
					if(j>0)
						if(m.args.get(j-1)=='L')
							nf.lvpt[j]= (int) mlv[j];
					nf.lv[j] = mlv[j];
				}

				nf.setMethod((Method) m);
				//nf = cF.newRefFrame(m);
				//oref = (int) lv[0];
				if (m.isAbstract()) {
					m = ((Instance) Heap.get(oref)).getMyClass().getMethod(m.getSignature());
				}
				//nf.code = m.getCode();
				cF = nf;
				pc = cF.pc;
				lv = cF.lv;

				cC = m.getMyClass();
				if (classa != null)
					classa.setFrame(cF);
				stack.push(cF);
				if(cF.code_type!= Method.BYTE_CODE)
					return cF.code_type;
				code = (byte[]) cF.code;
				#ifdef BYTECODE_PRINTER
				cout<<"invokeinterface"<<endl;
				#endif
				continue;
			case 0xBA:
				//invokedynamic
				#ifdef BYTECODE_PRINTER
				cout<<"invokedynamic"<<endl;
				#endif
				throw new RuntimeException("invokedynamic not implemented");
				*/
			case 0xBB:
				//_new
				cout<<"in new"<<endl;
				{
					scode=(unsigned short *)(code+pc+1);
					cout<<"new "<<bswap_16(scode[0])<<endl;
					Instance * c = new ClassInstance(cC->resolveClass(bswap_16(scode[0])));
					int a = heap->add(c);
					cF->push(a);
				}
				pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"new"<<endl;
				#endif
				continue;
				/*
			case 0xBC:
				//newarray
				//i1=size
				i1 = (int) cF.pop();
				array = new ArrayInstance(i1, code[pc + 1]);
				#ifdef BYTECODE_PRINTER
				cout<<""<<endl;
				#endif
				aref = Heap.add(array);
				cF.push(aref);
				pc += 2;
				#ifdef BYTECODE_PRINTER
				cout<<"newarray"<<endl;
				#endif
				continue;
			case 0xBD:
				//anewarray
				i1 = (int) cF.pop();
				array = new ArrayInstance(i1, ArrayInstance.T_OBJECT);
				aref = Heap.add(array);
				cF.push(aref);
				pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"anewarray"<<endl;
				#endif
				continue;
			case 0xBE:
				//arraylength
				aref = (int) cF.pop();
				array = (ArrayInstance) Heap.get(aref);
				cF.push(array.getLength());
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"arraylength"<<endl;
				#endif
				continue;
			case 0xBF:
				//athrow
				oref = (int) cF.pop();
				throwException(pc,oref);
				cF=stack.peek();
				if(cF.code_type!= Method.OPTIMIZED_BYTE_CODE)
					return cF.code_type;
				code = (byte[]) cF.code;
				lv = cF.lv;

				#ifdef BYTECODE_PRINTER
				cout<<"athrow"<<endl;
				#endif
				continue;
			case 0xC0:
				//checkcast
				fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
				fc = cC.getConstant(fc.index1);
				if (!fc.str.startsWith("[")) {
					AClass cl2 = Class.getClass(fc.str);
					i1 = (int) cF.peek();
					obj = Heap.get(i1);

					if (obj instanceof ClassInstance) {
						cl = ((ClassInstance) obj).getMyClass();
						if (!cl.isInstance(cl2)) {
							//ClassCastException
							throw new RuntimeException("CLASSCASTEXCEPTION: " + cl.getName() + " is not " + cl2.getName());
						}
					}
				}
				pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"checkcast"<<endl;
				#endif
				continue;
			case 0xC1:
				//instanceof
				i1 = (int) cF.pop();
				if (i1 == 0) {
					cF.push(0);
					pc += 3;
					continue;
				}
				obj = Heap.get(i1);
				i1 = ii(code[pc + 1], code[pc + 2]);
				AClass cl2 = cC.getClass(i1);
				cl = ((ClassInstance) obj).getMyClass();
				if (cl.isInstance(cl2))
					cF.push(1);
				else
					cF.push(0);
				pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"instanceof"<<endl;
				#endif
				continue;
			case 0xC2:
				//monitorenter
				oref = (int) cF.pop();
				Instance i=(Instance)Heap.get(oref);
				if(i.monitorenter(thread))
				{
					thread._wait();
					return 1;
				}
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"monitorenter"<<endl;
				#endif
				continue;
			case 0xC3:
				//monitorexit
				oref = (int) cF.pop();
				Instance _i=(Instance)Heap.get(oref);
				MyThread next=_i.monitorexit(thread);
				if(next!=null)
					JVM.notify(next);
				pc += 1;
				#ifdef BYTECODE_PRINTER
				cout<<"monitorexit"<<endl;
				#endif
				continue;

			case 0xC4:
				//wide
				pc += 1;
				switch (code[pc]) {
					case 0x15:
						//iload
						cF.push(lv[ii(code[pc + 1], code[pc + 2])]);
						pc += 3;
						continue;
					case 0x16:
						//lload
						i1 = ii(code[pc + 1], code[pc + 2]);
						cF.push(lv[i1 + 1]);
						cF.push(lv[i1]);
						pc += 3;
						continue;
					case 0x17:
						//fload
						cF.push(lv[ii(code[pc + 1], code[pc + 2])]);
						pc += 3;
						continue;
					case 0x18:
						//dload
						i1 = ii(code[pc + 1], code[pc + 2]);
						cF.push(lv[i1 + 1]);
						cF.push(lv[i1]);
						pc += 3;
						continue;
					case 0x19:
						//aload
						cF.push(lv[ii(code[pc + 1], code[pc + 2])]);
						pc += 3;
						continue;
					case 0x36:
						//istore
						lv[ii(code[pc + 1], code[pc + 2])] = cF.pop();
						pc += 3;
						continue;
					case 0x37:
						//lstore
						i1 = ii(code[pc + 1], code[pc + 2]);
						lv[i1] = cF.pop();
						lv[i1 + 1] = cF.pop();
						pc += 3;
						continue;
					case 0x38:
						//fstore
						lv[ii(code[pc + 1], code[pc + 2])] = cF.pop();
						pc += 3;
						continue;
					case 0x39:
						//dstore
						i1 = ii(code[pc + 1], code[pc + 2]);
						lv[i1] = cF.pop();
						lv[i1 + 1] = cF.pop();
						pc += 3;
						continue;
					case 0x3A:
						//astore
						oref = (int) cF.pop();
						lv[ii(code[pc + 1], code[pc + 2])] = oref;
						cF.lvpt[ii(code[pc + 1], code[pc + 2])] = oref;
						pc += 3;
						continue;
					case 0x84:
						//iinc
						i1 = (int) lv[ii(code[pc + 1], code[pc + 2])];
						lv[(int) code[pc + 1]] = i1 + ii(code[pc + 3], code[pc + 4]);
						pc += 5;
						continue;

				}
				throw new RuntimeException("wide not implememted");
				#ifdef BYTECODE_PRINTER
				cout<<"wide"<<endl;
				#endif
				//continue;
			case 0xC5:
				//multianewarray
				i1 = i(code[pc + 1], code[pc + 2]);
				//dimensions
				i2 = code[pc + 1];
				pc += 4;
				throw new RuntimeException("not implememted");
				#ifdef BYTECODE_PRINTER
				cout<<"multianewarray"<<endl;
				#endif
				//continue;
			case 0xC6:
				//ifnull
				if ((int) cF.pop() == 0)
					pc += i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"ifnull"<<endl;
				#endif
				continue;
			case 0xC7:
				//ifnonnull
				if ((int) cF.pop() != 0)
					pc += i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				#ifdef BYTECODE_PRINTER
				cout<<"ifnonnull"<<endl;
				#endif
				continue;
			case 0xC8:
				//goto
				pc += i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
				#ifdef BYTECODE_PRINTER
				cout<<"goto"<<endl;
				#endif
				continue;
			case 0xC9:
				//jsr_w
				cF.push(pc + 5);
				pc += i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
				#ifdef BYTECODE_PRINTER
				cout<<"jsr_w"<<endl;
				#endif
				continue;
				*/
			default:
				cout<< "opcode: " << std::hex << ((int)code[pc])<<endl;
				throw ("wrong opcode");
				// cs+= byteToHex(code[i])+"\n";
		}
	} while (true);
	//} while (steps-->0);
cF->pc = pc;
return 0;
}

/*
	private int l2i1(long l) {
		return (int) (l >> 32);
	}

	private int l2i2(long l) {
		return (int) l;
	}


	private static short i(byte b1, byte b2) {
		return (short) (((b1 & 0xff) << 8) | (b2 & 0xff));
	}

	private static long _2i2l(int i1, int i2) {
		return ((Integer) i2).longValue() << 32 | ((Integer) (i1)).longValue() & 0xFFFFFFFFL;
	}

	private static int ii(byte b1, byte b2) {
		return (((b1 & 0xff) << 8) | (b2 & 0xff));
	}

	private static int i(byte b1, byte b2, byte b3, byte b4) {
		return ((0xFF & b1) << 24) | ((0xFF & b2) << 16) |
				((0xFF & b3) << 8) | (0xFF & b4);
	}

	public Frame getCurrentFrame() {
		return stack.peek();
	}

	public FrameStack getStack() {
		return stack;
	}

	public void checkReferences(Set<Integer> refs) {
		Iterator<Frame> it = stack.iterator();
		Frame f = it.next();
		while(f!=null){
			f.checkReference(refs);
			f=it.next();
		}
	}
}
	*/
