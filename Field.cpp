#include<string>
#include<iostream>

#include "lang/lang.h"

#include"include/ByteBuffer.h"
#include"include/Constant.h"
#include"include/Attribute.h"
#include"include/Field.h"
#include"include/MException.h"
#include"include/CodeAttribute.h"
#include"include/Method.h"
#include"include/Frame.h"
#include"include/Class.h"

using namespace std; 

Field::Field(Class * c, ByteBuffer * buf){
	access_flags = buf->getUShort();
	int name_index = buf->getUShort();

	//name = c.getConstant(name_index).str;

	int descriptor_index = buf->getUShort();
	//int di=(int)descriptor_index;

	//desc = c.getConstant(descriptor_index).str;


	int attributes_count = buf->getUShort();
	//attrs = new ArrayList<>(attributes_count);
	for (int ix = 0; ix < attributes_count; ix++) {
		Attribute * a = Attribute::generate(c, buf);
		/*String name2 = c.getConstant(a.attribute_name_index).str;
		  if (name2.equals("ConstantValue"))
		  constant_value = new ConstantValueAttribute(a, c);
		  */
		//attrs.add(a);
	}
}
