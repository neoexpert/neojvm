#include <cstdint>
#include <iostream>
#include "../lang/include/Object.h"
#include "include/Array.h"
#include "include/HashMap.h"
#include "include/HashSet.h"

using namespace std;

template<typename V>
Object * HashSet<V>::dummy=new Object();
template<typename V>
HashSet<V>::HashSet(){
	map=new HashMap<V,Object>();
}

template<typename V>
HashSet<V>::~HashSet(){
	delete map;
	//delete dummy;?
}

template<typename V>
void HashSet<V>::add(V * value){
	map->put(value, dummy);
}

template<typename V>
void HashSet<V>::remove(V * value){
	map->remove(value);
}

template<typename V>
bool HashSet<V>::contains(V * value){
	return map->containsKey(value);
}

template<typename V>
void HashSet<V>::clear(){
	map->clear();
}

template<typename V>
int HashSet<V>::size(){
	return map->size();
}
