#include <string>
#include <iostream>
#include <cstdint>
#include "../lang/include/Object.h"
#include "include/PArray.h"

using namespace std;

PArray::PArray(char type, int length):type(type),length(length){
	switch(type){
		case 'I':
			arr=new int[length]();
			break;
		case 'Z':
			arr=(int *)new bool[length]();
			break;
		case 'B':
			arr=(int *)new char[length]();
			break;
		case 'C':
			arr=(int *)new unsigned short[length]();
			break;
		case 'D':
			arr=(int *)new double[length]();
			break;
		case 'F':
			arr=(int *)new float[length]();
			break;
		case 'J':
			arr=(int *)new long[length]();
			break;
		case 'S':
			arr=(int *)new short[length]();
			break;
		/*
		case 'L':
			arr=(int *)new Object ** [length]();
			break;
		*/
		default:
			throw "unknown type";
	}
}

Object * PArray::clone(){
	PArray * new_array=new PArray(type,length);
	int size;
	switch(type){
		case 'I':
			size=length*4;
			break;
		case 'Z':
			size=length*1;
			break;
		case 'B':
			size=length*1;
			break;
		case 'C':
			size=length*2;
			break;
		case 'D':
			size=length*8;
			break;
		case 'F':
			size=length*4;
			break;
		case 'J':
			size=length*8;
			break;
		case 'S':
			size=length*2;
			break;
	}
	char * target = (char*)new_array->arr;
	char * source = (char*)arr;
	for(int i=0;i<size;i++)
	{
		target[i]=source[i];
	}
	return new_array;

}

PArray::~PArray(){
	delete[] arr;
}
