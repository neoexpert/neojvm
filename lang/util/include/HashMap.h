#ifndef _HashMap_h_
#define _HashMap_h_

using namespace std;
template<typename K, typename V>
class Node:public Object{
	public:
		Node(K * key, V * value){
			this->key=key;
			this->value=value;
		}
		virtual ~Node(){
			delete key;
			delete value;
		}
		K * key;
		V * value;
};

template<typename K, typename V>
class HashMap:public Object
{ 
	private:
		Array<Node<K,V>> * values;
		int _size=0;
		void increase(int new_size){
			Array<Node<K,V>> * new_values=new Array<Node<K,V>>(new_size);
			Node<K,V> ** vs=(Node<K,V> **)values->arr;
			int length=values->length;
			for(int i=0;i<length;i++){
				Node<K,V> * n = vs[i];
				if(n!=0){
					if(!putNode(new_values,n))
					{
						increase(new_size*2);
						throw "check me";
						return;
					}
				}
			}
			delete values;
			values=new_values;
		}
		bool putNode(Array<Node<K,V>> * arr, Node<K,V> * node){
			Node<K,V> ** vs=(Node<K,V> **)arr->arr;
			int hash=node->key->hashCode();
			int n=arr->length;
			int pos=(hash % n + n) % n;
			Node<K,V> * o = vs[pos];
			if(o==0)
			{
				vs[pos] = node;
				return true;
			}
			else{
				if(o->key->equals(node->key))
				{
					throw "ERROR in HashMap";
				}
				return false;
			}
		}
	public:
		HashMap(){
			values=new Array<Node<K,V>>(8);
		}

		~HashMap(){
			values->delete_elements();
			delete values;
		}

		void put(K * key, V * value){
			Node<K,V> ** vs=(Node<K,V> **)values->arr;
			int hash=key->hashCode();
			int n=values->length;
			int pos=(hash % n + n) % n;
			Node<K,V> * o = vs[pos];
			if(o==0)
			{	//collision
				vs[pos] = new Node<K,V>(key, value);
				_size++;
				return;
			}
			else{
				if(o->key->equals(key))
				{
					o->value=value;;
					return;
				}
				increase(values->length*2);
				put(key,value);
			}
		}

		V * get(K * key){
			Node<K,V> ** a=(Node<K,V> **)values->arr;
			int hash=key->hashCode();
			int n=values->length;
			int pos=(hash % n + n) % n;
			Node<K,V> * node=a[pos];
			if(node!=0)
			{
				if(node->key->equals(key))
					return node->value;
			}
			return 0;
		}

		bool containsKey(K * key){
			Node<K,V> ** a=(Node<K,V> **)values->arr;
			int hash=key->hashCode();
			int n=values->length;
			int pos=(hash % n + n) % n;
			Node<K,V> * node=a[pos];
			if(node!=0)
				return node->key->equals(key);
			return false;
		}

		void remove(K * key){
			Node<K,V> ** a=(Node<K,V> **)values->arr;
			int hash=key->hashCode();
			int n=values->length;
			int pos=(hash % n + n) % n;
			Node<K,V> * removed = a[pos];
			a[pos]=0;
			_size--;
			delete removed;
			//return removed->value;
		}

		void clear(){
			//values->delete_elements();
			delete values;
			values=new Array<Node<K,V>>(8);
			_size=0;
		}

		int size(){
			return _size;
		}
};
#endif
