#ifndef _ArrayList_h_
#define _ArrayList_h_

using namespace std;

template<typename V>
class ArrayList:public Object
{ 
	private:
		int _size=0;
	public:
		Array<V> * array;
		ArrayList():ArrayList(8){}

		ArrayList(int initialsize){
			array=new Array<V>(initialsize);
		}
		virtual ~ArrayList(){
			delete array;
		}

		void add(V * v){
			if(_size>=array->length){
				Array<V> * new_array=new Array<V>(array->length*2);
				for(int i=0;i<array->length;i++)
					new_array->set(i,array->get(i));
				delete array;
				array=new_array;
			}
			array->set(_size++,v);
		}

		V * get(int i){
			return array->get(i);
		}
		int size(){
			return _size;
		}
};
#endif
