#ifndef _HashSet_h_
#define _HashSet_h_

using namespace std;

template<typename V>
class HashSet:public Object
{ 
	private:
		HashMap<V,Object> * map;
		static Object * dummy;
	public:
		HashSet();
		~HashSet();
		void add(V * value);
		Object * get(V * value);
		void remove(V * value);
		bool contains(V * value);
		void clear();
		int size();
};
#endif
