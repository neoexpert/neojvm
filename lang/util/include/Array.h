#ifndef _Array_h_
#define _Array_h_

using namespace std;

template<typename V>
class Array:public Object
{ 
	private:
	public:
		const int length;
		V ** arr;
		Array(int length):length(length){
			arr=new V*[length]();
		}
		virtual ~Array(){
			//don't forget to delete elements
			//delete_elements();
			delete[] arr;
		}
		void set(int i, V * v){
			arr[i]=v;
		}

		V * get(int i){
			return arr[i];
		}

		void delete_elements(){
			Object ** a=(Object **)arr;
			for(int i=0;i<length;i++){
				delete a[i];
			}
		}
		virtual Object * clone(){
			Array<V> * new_array=new Array<V>(length);
			V ** target = new_array->arr;
			V ** source = arr;
			for(int i=0;i<length;i++)
			{
				target[i]=source[i];
			}
			return new_array;
		}

};
#endif
