#ifndef _PArray_h_
#define _PArray_h_

using namespace std;

//Array for primitives
class PArray:public Object
{ 
	private:
	public:
		const int length;
		const char type;
		int * arr;
		PArray(char type, int length);
		virtual ~PArray();
		virtual Object * clone();

};
#endif
