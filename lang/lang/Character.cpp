#include <cstdint>
#include "include/Object.h"
#include "include/Character.h"


bool Character::equals(Object * o){
	if(this==o)
		return true;
	//instanceof
	if(Character * i = dynamic_cast<Character*>(o)){
		return i->value==value;
	}
	return false;
}
Character * _(char c){
	return new Character(c);
}
