#include <cstdint>
#include "include/Object.h"
#include "include/Integer.h"


bool Integer::equals(Object * o){
	if(this==o)
		return true;
	//instanceof
	if(Integer * i = dynamic_cast<Integer*>(o)){
		return i->value==value;
	}
	return false;
}
Integer * _(int i){
	return new Integer(i);
}
