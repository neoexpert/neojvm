#include <string>
#include <iostream>
#include <cstdint>
#include "include/Object.h"
#include "include/Character.h"
#include "../util/include/Array.h"
#include "../util/include/PArray.h"
#include "include/StringLatin1.h"
#include "include/StringUTF16.h"
#include "../util/include/HashMap.h"
#include "include/String.h"

using namespace std;

HashMap<String,String> * String::interned=new HashMap<String,String>();

String::String(){
	this->value=new PArray('C',0);
}
String::String(int length){
	this->value=new PArray('C',length);
}
String::~String(){
	delete value;
}
String::String(PArray * value){
	this->value=value;
	coder=1;
}
String::String(string s){
	this->value=new PArray('B',s.size());
	s.copy((char *)this->value->arr, s.size());
}

int String::hashCode(){
	int h = hash;
    if (h == 0 && value->length > 0) {
        hash = h = isLatin1() ? StringLatin1::hashCode(value)
            : StringUTF16::hashCode(value);
    }
    return h;
}

bool String::equals(Object * o){
	if(this==o)
		return true;
	//instanceof
	if(String * so = dynamic_cast<String*>(o)){
		if(value->length!=so->value->length)
			return false;
		char * arr=(char*)value->arr;
		char * oarr=(char*)so->value->arr;
		int length=value->length;
		for(int i=0;i<length;i++)
			if(arr[i]!=oarr[i])
				return false;
		return true;

	}
	return false;
}

string * String::to_cpp_string(){
	char * arr = (char *) value->arr;
	return new string(arr,value->length);
}

String * String::intern(){
	if(interned->containsKey(this))
		return (String *)interned->get(this);
	interned->put(this,this);
	return this;
}

//static
String * String::_(string cppstring){
	String * s = new String(cppstring);
	String * is = s->intern();
	if(s!=is) delete s;
	return is;
}

int String::indexOf(char c){
	char * arr=(char*)value->arr;
	for(int i=0;i<value->length;i++){
		if(arr[i]==c)
			return i;
	}
	return -1;
}

String * String::substring(int index){
	PArray * v=new PArray('C',value->length-index);
	char * source=(char *)value->arr;
	char * target=(char *)v->arr;
	for(int i=index;i<value->length;i++)
		target[i-index]=source[i];
	return new String(v);
}


bool String::isLatin1(){
	return true;
}

String * _(string cpp_string){
	String * s = new String(cpp_string);
	String * is = s->intern();
	if(s!=is) delete s;
	return is;
}

std::ostream& operator<< (std::ostream &out, const String &s)
{
	int length=s.value->length;
	char * ca=(char *)s.value->arr;
	for(int i=0;i<length;i++)
		out<<ca[i];
	return out;
}
