#ifndef _Object_h_
#define _Object_h_

using namespace std;

class Object
{ 
	protected:
		constexpr static Object * null=0;
	public:
		virtual ~Object();
		virtual int hashCode();
		virtual Object * clone();
		virtual bool equals(Object * o);
};
#endif
