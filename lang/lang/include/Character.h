#ifndef _Character_h_
#define _Character_h_


class Character:public Object
{ 
	public:
		Character(char value):value(value){}
		const char value;
		virtual int hashCode(){return value;}
		virtual bool equals(Object * o);
		operator char()
   		{
      			return value;
   		}
};
Character * _(char c);
#endif
