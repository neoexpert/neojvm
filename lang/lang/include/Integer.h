#ifndef _Integer_h_
#define _Integer_h_

class Integer:public Object
{ 
	public:
		Integer(int value):value(value){}
		const int value;
		virtual int hashCode(){return value;}
		virtual bool equals(Object * o);
		operator int()
   		{
      			return value;
   		}
};
Integer * _(int i);
#endif
