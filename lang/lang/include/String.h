#ifndef _String_h_
#define _String_h_

using namespace std;

class String:public Object
{ 
	private:
		int hash=0;
		PArray * value;
		bool isLatin1();
		const char LATIN1 = 0;
		char coder;
		static HashMap<String, String> * interned;
		String(int length);
	public:
		String();
		~String();
		String(PArray * value);
		String(string cppstring);
		String operator+(String & second)
		{
			int this_length=this->length();
			int second_length=second.length();
			int result_length=this_length+second_length;
			String * result = new String(result_length);
			char * caresult=(char *)result->value->arr;
			char * ca_this=(char *)this->value->arr;
			char * ca_second=(char *)second.value->arr;
			for(int i=0;i<this_length;i++)
				caresult[i]=ca_this[i];
			for(int i=this_length;i<result_length;i++)
				caresult[i]=ca_second[i-this_length];
			return *result;
		}
		String & operator=(const String &other){
			//if(this!=other){
			this->value=(PArray *)other.value->clone();
			this->hash=other.hash;
			//}
			return *this;
		}

		friend std::ostream& operator<<(std::ostream&, const String &);
		String * intern();
		int length(){
			return value->length;
		}
		int indexOf(char c);
		String * substring(int index);
		Character * charAt(int index){
			char * arr=(char *)value->arr;
			return new Character(arr[index]);
		}
		virtual int hashCode();
		virtual bool equals(Object * o);
		string * to_cpp_string();
		static String * _(string s);
};
//std::ostream& operator<< (std::ostream &out, const String &s);
String * _(string cpp_string);
#endif
