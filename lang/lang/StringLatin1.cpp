#include <string>
#include <cstdint>
#include "include/Object.h"
#include "../util/include/PArray.h"
#include "include/StringLatin1.h"

using namespace std;

int StringLatin1::hashCode(PArray * value){
	char * arr = (char *) value->arr;
	int length=value->length;
	int h = 0;
        for (int i=0;i<length;i++) {
        	char v=arr[i];
            h = 31 * h + (v & 0xff);
        }
    return h;
}

